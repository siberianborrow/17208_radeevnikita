import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;

public class KeyAttachment {
  public SelectionKey partner;
  public ByteBuffer toread;
  public ByteBuffer towrite;
}
