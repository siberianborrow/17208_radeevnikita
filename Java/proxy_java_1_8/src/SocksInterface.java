public interface SocksInterface {
    void connect();
    void bind();
    byte getSocksCommand();
    void getClientCommand();
    void udpAssociate();
}
