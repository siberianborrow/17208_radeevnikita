public class Constants {
    public static final int PROXY_PORT = 1080;
    public static final int C_CONNECT = 1;
    public static final int C_BIND = 2;
    public static final int C_UDP = 3;
    public static final byte CODE_SUCCESS = 90;
    public static final int BUF_SIZE = 16384;
    public static final int DEFAULT_PROXY_TIMEOUT = 10;
}
