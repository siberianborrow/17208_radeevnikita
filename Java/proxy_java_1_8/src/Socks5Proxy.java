import java.io.IOException;
import java.net.InetAddress;
import java.util.Arrays;

public class Socks5Proxy implements SocksInterface {
    private byte socksCommand;
    private byte[] destPort;

    private byte nmethods = 0;
    private byte[] methods;
    private ProxyThread parentThread = null;
    private int f_clientPort;
    private int f_destPort;
    private InetAddress f_clientADDR;
    private InetAddress f_desttADDR;
    private byte adressType = 0;
    private byte REP = 0;


    public Socks5Proxy(ProxyThread parent)
    {
        parentThread = parent;
        f_clientPort = parent.getClientSocket().getPort();
        f_clientADDR = parent.getClientSocket().getInetAddress();
        destPort = new byte[2];
    }
    public byte getSocksCommand()
    {
        return socksCommand;
    }
    public void getClientCommand()
    {
        nmethods = getByte();//NMETHODS
        methods = new byte[nmethods];//METHODS
        for(int i = 0; i < nmethods; ++i)
            methods[i] = getByte();
        chooseMethod();
        if(getByte() == 5)
        {
            socksCommand = getByte();
            if(getByte() == 0)//read reserved byte
            {
                adressType = getByte();
                getAdress();
                destPort[0] = getByte();
                destPort[1] = getByte();
                f_destPort = Utilities.decodeDestPort(destPort[0], destPort[1]);
            }
            else
            {
                //!@ incorrect reserved
            }
        }
        else
        {
            //!@ incorrect version
        }
    }
    public void getAdress()
    {
        byte[] destAddress;
        switch (adressType)
        {
            case 1:
                destAddress = new byte[4];
                destAddress[0] = getByte();
                destAddress[1] = getByte();
                destAddress[2] = getByte();
                destAddress[3] = getByte();
                f_desttADDR = Utilities.decodeDestIPv4(destAddress);
                break;
            case 3:
                byte len = getByte();
                destAddress = new byte[len];
                for(int i = 0; i < len; ++i)
                    destAddress[i] = getByte();
                String addressString = new String(destAddress,0, len);
                f_desttADDR = Utilities.decodeDestHostName(addressString);
                break;
            case 4:
                destAddress = new byte[16];
                for(int i = 0; i < 16; ++i)
                    destAddress[i] = getByte();
                f_desttADDR = Utilities.decodeDestIPv6(destAddress);
                break;
        }
        if(f_desttADDR == null)
        {
            //!@ close
        }
    }
    public void chooseMethod()
    {
        //!@ write it right
        byte meth = 0;
        sendMethod(meth);//0 means "no authentication required"
    }
    public void sendMethod(byte meth)
    {
        try
        {
            parentThread.getClientOutput().write(5);//VER
            parentThread.getClientOutput().write(meth);//METHOD
            System.out.println("Method (" + meth + ") was  send");
        }
        catch(IOException e)
        {
            System.out.println("sendMethod IOException " + e);
        }
    }
    private byte getByte()
    {
        byte b = 0;
        try
        {
            b = parentThread.getByteFromClient();
            return b;
        }
        catch(Exception e)
        {
            //!@close it
        }
        return b;
    }
    public void replyClientCommand()
    {
        byte [] answer = new byte[10];//!@ 10 is only for IPv4
        answer[0] = 5;//version of socks
        answer[1] = REP;//reply field
        answer[2] = 0;//reserved
        answer[3] = 1;//IPv4 it could be that this must be rewrited for ipv6 or hostname
        byte [] ip = f_clientADDR.getAddress();
        answer[4] = ip[0];
        answer[5] = ip[1];
        answer[6] = ip[2];
        answer[7] = ip[3];
        answer[8] = (byte)(f_clientPort >> 8);
        answer[9] = (byte)f_clientPort;
        try {
            parentThread.getClientOutput().write(answer);
            parentThread.getClientOutput().flush();
            System.out.println("Answer (" + Arrays.toString(answer) + ") was  send");
        }
        catch(IOException e)
        {
            System.out.println("replyClientCommand IOException " + e);
        }
    }
    public void udpAssociate()
    {

    }
    public void connect()
    {
        System.out.println(f_clientPort + " connecting...");
        parentThread.connectToServer(f_desttADDR, f_destPort);
        System.out.println("Connected successfully");
        replyClientCommand();
    }
    public void bind()
    {}
}
