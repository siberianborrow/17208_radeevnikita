import java.io.IOException;
import java.net.InetAddress;
import java.util.Arrays;

public class Socks4Proxy implements SocksInterface {
    private byte socksCommand;
    private byte[] destPort;
    private byte[] destIP;
    ProxyThread parentThread;
    private int f_clientPort;
    private int f_destPort;
    private InetAddress f_desttIP;

    Socks4Proxy(ProxyThread parent)
    {
        parentThread = parent;
        f_clientPort = parent.getClientSocket().getPort();
        destPort = new byte[2];
        destIP = new byte[4];
    }

    public byte getSocksCommand() {
        return socksCommand;
    }

    private byte getByte()
    {
        byte b = 0;
        try
        {
            b = parentThread.getByteFromClient();
            return b;
        }
        catch(Exception e)
        {
            //!@close it
        }
        return b;
    }
    public void getClientCommand()
    {
        socksCommand = getByte();
        destPort[0] = getByte();
        destPort[1] = getByte();
        for(int i = 0; i < 4; ++i) {
            destIP[i] = getByte();
        }
        f_destPort = Utilities.decodeDestPort(destPort[0], destPort[1]);
        f_desttIP = Utilities.decodeDestIPv4(destIP);
        byte trash = 1;
        while(trash != 0) {
            trash = getByte();
            System.out.println("trash was read");
        }
        if((socksCommand != Constants.C_CONNECT && socksCommand != Constants.C_BIND) || f_destPort < 0 || f_desttIP == null)
        {
            System.out.println("getClientCommand close block for " + f_clientPort);
        }
    }
    public void replyClientCommand(byte code)
    {
        byte [] answer = new byte[8];
        answer[0] = 0;
        answer[1] = code;
        answer[2] = destPort[0];
        answer[3] = destPort[1];
        answer[4] = destIP[0];
        answer[5] = destIP[1];
        answer[6] = destIP[2];
        answer[7] = destIP[3];
        try {
            parentThread.getClientOutput().write(answer);
            parentThread.getClientOutput().flush();
            System.out.println("Answer (" + Arrays.toString(answer) + ") was  send");
        }
        catch(IOException e)
        {
            System.out.println("replyClientCommand IOException " + e);
        }
    }
    public void connect()
    {
        System.out.println(f_clientPort + " connecting...");
        parentThread.connectToServer(f_desttIP, f_destPort);
        System.out.println("Connected successfully");
        replyClientCommand(Constants.CODE_SUCCESS);
    }
    public void bind()
    {}
    public void udpAssociate()
    {
        ;//socks 4 hasn't support of udp
    }
}
