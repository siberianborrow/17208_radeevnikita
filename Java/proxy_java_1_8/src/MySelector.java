import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;

public class MySelector implements Runnable {
  private Selector mainSelector = null;
  private ServerSocketChannel servSock = null;
  private boolean flagLogs = false;

  public MySelector() {}

  public void run() {
    try {
      mainSelector = Selector.open();
      InetAddress host = InetAddress.getByName("localhost");
      servSock = ServerSocketChannel.open();
      servSock.configureBlocking(false);
      servSock.bind(new InetSocketAddress(host, Constants.PROXY_PORT));
      servSock.register(mainSelector, servSock.validOps());
      System.out.println("MySelector started work");
    } catch (IOException e) {
      System.out.println("ServerSocketChannel.open() IOException: " + e);
    }
    try {
      while (mainSelector.select() != -1)
      {
        Iterator<SelectionKey> iterator = mainSelector.selectedKeys().iterator();
        while (iterator.hasNext()) {
          SelectionKey key = iterator.next();
          iterator.remove();
          if (key.isValid()) {
            if (key.isAcceptable())
              accept(key);
            else if (key.isConnectable())
              connect(key);
            else if (key.isReadable())
              read(key);
            else if (key.isWritable())
              write(key);
            else
              close(key);
          }
        }
      }
    } catch (IOException e) {
      System.out.println("mainSelector cycle IOException: " + e);
    }
  }

  private void accept(SelectionKey key) {
    try {
      SocketChannel channel = ((ServerSocketChannel)key.channel()).accept();
      channel.configureBlocking(false);
      channel.register(mainSelector, SelectionKey.OP_READ);
      if(flagLogs)
        System.out.println("Accept function has worked");
    }
    catch(IOException e)
    {
      System.out.println("servSock.accept() IOException: " + e);
    }
  }
  private void connect(SelectionKey key) {
    SocketChannel channel = (SocketChannel) key.channel();
    KeyAttachment attachment = (KeyAttachment) key.attachment();
    try {
      channel.finishConnect();
      attachment.toread = ByteBuffer.allocate(Constants.BUF_SIZE);
      byte [] answer = {0, 90, 0, 0, 0, 0, 0, 0};
      attachment.toread.put(answer).flip();
      attachment.towrite = ((KeyAttachment)attachment.partner.attachment()).toread;
      ((KeyAttachment) attachment.partner.attachment()).towrite = attachment.toread;
      attachment.partner.interestOps(SelectionKey.OP_READ | SelectionKey.OP_WRITE);
      key.interestOps(0);
      if(flagLogs)
        System.out.println("Connect has worked");
    }
    catch(IOException e)
    {
      System.out.println("finishConnect() IOEception: " + e);
    }
  }

  private void readHead(KeyAttachment attachment, SelectionKey key)
  {
    try {
      SocketChannel partner = SocketChannel.open();
      partner.configureBlocking(false);

      byte[] tmp = {attachment.toread.array()[4], attachment.toread.array()[5], attachment.toread.array()[6], attachment.toread.array()[7]};
      InetAddress addr = InetAddress.getByAddress(tmp);

      int port = Utilities.decodeDestPort(attachment.toread.array()[2], attachment.toread.array()[3]);
      partner.connect(new InetSocketAddress(addr, port));

      SelectionKey partnerKey = partner.register(mainSelector, SelectionKey.OP_CONNECT);

      key.interestOps(0);

      attachment.partner = partnerKey;
      KeyAttachment partnerAttachment = new KeyAttachment();
      partnerKey.attach(partnerAttachment);
      partnerAttachment.partner = key;

      attachment.toread.clear();
    }
    catch(IOException e)
    {
      System.out.println("readHead IOException: " + e);
    }
  }

  private KeyAttachment makeAttachment(SelectionKey key)
  {
    KeyAttachment attachment = (KeyAttachment)key.attachment();
    if(attachment == null)//there are not any connection
    {
      attachment = new KeyAttachment();
      key.attach(attachment);
      attachment.toread = ByteBuffer.allocate(Constants.BUF_SIZE);
    }
    return attachment;
  }

  private void read(SelectionKey key)
  {
    SocketChannel channel = (SocketChannel)key.channel();
    KeyAttachment attachment = makeAttachment(key);
      try {
        if (channel.read(attachment.toread) < 1)
          close(key);
        else if (attachment.partner == null)
        {
          if (attachment.toread.array()[1] == 1 && attachment.toread.array()[0] == 4 && attachment.toread.array().length > 9)
            readHead(attachment, key);
          else
          {
            System.out.println("Incorrect connection");
            close(key);
          }
        }
        else
        {
          attachment.partner.interestOps(attachment.partner.interestOps() | SelectionKey.OP_WRITE);//partner want to write
          key.interestOps(key.interestOps() ^ SelectionKey.OP_READ);//key doesn't want to read because data isn't read yet
          attachment.toread.flip();
        }
    }
      catch(IOException e)
      {
        System.out.println("SocketChannel read() IOException: " + e);
      }
      if(flagLogs)
        System.out.println("Read has worked");
  }

  private void write(SelectionKey key)
  {
    SocketChannel channel = (SocketChannel)key.channel();
    KeyAttachment attachment = (KeyAttachment)key.attachment();
    try {
      if (channel.write(attachment.towrite) == -1)
        close(key);
      else if(attachment.towrite.remaining() == 0)
      {
        if(attachment.partner == null)
          close(key);
        else
        {
          attachment.towrite.clear();
          key.interestOps(key.interestOps() ^ SelectionKey.OP_WRITE);
          attachment.partner.interestOps(attachment.partner.interestOps() | SelectionKey.OP_READ);
        }
      }
    }
    catch(IOException e)
    {
      System.out.println("SocketChannel write IOException: " + e);
    }
    if(flagLogs)
      System.out.println("Write has worked");
  }
  private void close(SelectionKey key)
  {
    key.cancel();
    try {
      key.channel().close();
      SelectionKey partner = ((KeyAttachment)key.attachment()).partner;
      if(partner != null)
      {
        ((KeyAttachment)partner.attachment()).partner = null;
        if((partner.interestOps() & SelectionKey.OP_WRITE) == 0)
          ((KeyAttachment)partner.attachment()).towrite.flip();
        partner.interestOps(SelectionKey.OP_WRITE);
      }
    }
    catch(IOException e)
    {
      System.out.println("close() IOException: " + e);
    }
    if(flagLogs)
      System.out.println("Close has worked");
  }
}
