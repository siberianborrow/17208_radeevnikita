import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.util.Iterator;

public class Server {
  private static byte threadMode = 1;//mode of threads (one or multi)

  public static void main(String[] args) {


    if(threadMode == 0)//multithread
    {
      ServerSocket servSocket = null;
      try
      {
        servSocket = new ServerSocket(Constants.PROXY_PORT);
        System.out.println("Server started work");
      }
      catch(IOException e)
      {
        System.out.println("ServerSocket IOException: " + e);
      }

      Socket clientSocket = null;
      do
        {
        try
        {
          clientSocket = servSocket.accept();
        }
        catch (IOException e)
        {
          System.out.println("servSocket.accept() IOException: " + e);
        }

        if (clientSocket != null) {
          new Thread(new ProxyThread(clientSocket)).run();
          //addConnection(clientSocket);//add connection,when it will be finished
          //node from map will be deleted
        }

      } while (!isStopped());//!@ this doesn't work
    }
    else//one thread
    {
      new Thread(new MySelector()).run();
    }
  }
  private static boolean isStopped()//!@ write it
  {
    return false;
  }
}