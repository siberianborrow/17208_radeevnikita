import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Utilities {
    public static int decodeDestPort(byte firstByte, byte secondByte)
    {
        int answer1 = firstByte;
        if(answer1 < 0) answer1 = (0x100 + answer1);
        int answer2 = secondByte;
        if(answer2 < 0) answer2 = (0x100 + answer2);
        int answer = (answer1 << 8) | answer2;
        return answer;
    }
    public static InetAddress decodeDestIPv4(byte [] DestIP)
    {
        InetAddress ip;
        try
        {
            ip = InetAddress.getByAddress(DestIP);
        }
        catch(UnknownHostException e)
        {
            return null;
        }
        return ip;
    }
    public static InetAddress decodeDestHostName(String hostname)
    {
        InetAddress ip;
        try
        {
            ip = InetAddress.getByName(hostname);
        }
        catch(UnknownHostException e)
        {
            return null;
        }
        return ip;
    }
    public static InetAddress decodeDestIPv6(byte [] DestIP)
    {
        InetAddress ip;
        try
        {
            ip = Inet6Address.getByAddress(DestIP);
        }
        catch(UnknownHostException e)
        {
            return null;
        }
        return ip;
    }
}
