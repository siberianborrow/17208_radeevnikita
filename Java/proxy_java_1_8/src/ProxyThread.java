import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.HashMap;

public class ProxyThread implements Runnable
{
  private Socket clientSocket = null;
  private Socket servSock = null;
  private InputStream clientInput = null;
  private OutputStream clientOutput = null;
  private Thread clientServer = null;
  private Thread serverClient = null;
  private byte version = 0;
  private SocksInterface socks = null;
  private HashMap<Integer, Socket> opennedStreams = new HashMap<>();

  public ProxyThread(Socket clientSock)
  {
    clientSocket = clientSock;
  }

  public OutputStream getClientOutput() {
    return clientOutput;
  }

  public Socket getClientSocket() {
    return clientSocket;
  }

  public void run()
  {
      try {
          clientInput = clientSocket.getInputStream();
          clientOutput = clientSocket.getOutputStream();
          version = getByteFromClient();//read Socks version
          if(version == 4)//Socks 4
          {
            socks = new Socks4Proxy(this);
          }
          else if(version == 5)//socks 5
          {
            socks = new Socks5Proxy(this);
          }
          else
          {
            System.out.println(clientSocket.getPort() + " isn't socks 4, closing connection.");
            close();
            return; //!@ here must be closing of program
          }
          socks.getClientCommand();
          switch (socks.getSocksCommand()){
            case Constants.C_CONNECT:
              socks.connect();
              clientServer = new Thread(new DataExchange(clientSocket, servSock, this));
              clientServer.start();
              serverClient = new Thread(new DataExchange(servSock, clientSocket, this));
              serverClient.start();
              opennedStreams.put(clientSocket.getPort(), clientSocket);
              opennedStreams.put(servSock.getPort(), servSock);
              break;
            case Constants.C_BIND:
              socks.bind();
              break;
            case Constants.C_UDP:
              socks.udpAssociate();
              break;
          }
      }
      catch(IOException e)
      {
          System.out.println("clientSocket.getInputStream() IOException " + e);
      }

  }
  public void deleteStream(Socket sock)
  {
    opennedStreams.remove(sock.getPort());
  }
  public void close()
  {
    try {
      if(clientInput != null)
      {
        clientInput.close();
      }
      if(clientOutput != null)
      {
        clientOutput.flush();
        clientOutput.close();
      }
      if(clientSocket != null) {
        deleteStream(clientSocket);
        clientSocket.close();
      }
      if(servSock != null) {
        deleteStream(servSock);
        servSock.close();
      }
      if(!clientServer.isInterrupted())//interrupt if it is not yet
        clientServer.interrupt();
      if(!serverClient.isInterrupted())
        serverClient.interrupt();
      if(clientServer != null)
        clientServer.join();
      if(serverClient != null)
        serverClient.join();
    }
    catch(Exception e)
    {
      System.out.println("close() in ProxyThread IOException: " + e);
    }
  }
  public byte getByteFromClient() {

    int buf = 0;
    try {
      buf = clientInput.read();
    }
    catch (IOException e) {
      System.out.println("getByteFromClient() IOException " + e);
      //!@ do something that has meaning
    }
    if (buf == -1)//I think it is impossible but let it be here
    {
        //!@ close connection
    }
    return (byte) buf;
  }


  public void connectToServer(InetAddress destIP, int port)
  {
    try
    {
      servSock = new Socket(destIP, port);
      System.out.println("Connected to " + destIP.getHostName());

    }
    catch(IOException e)
    {
      System.out.println("connectToServer IOException: " + e);
      //!@close
    }
  }
}