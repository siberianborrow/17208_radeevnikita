import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class DataExchange implements Runnable {
    private Socket sockInput = null;
    private Socket sockOutput = null;
    private InputStream input = null;
    private OutputStream output = null;

    private byte[] buffer = null;
    ProxyThread parent = null;

    public DataExchange(Socket socketInput, Socket socketOutput, ProxyThread parentThread)
    {
        sockInput = socketInput;
        sockOutput = socketOutput;
        buffer = new byte[Constants.BUF_SIZE];
        parent = parentThread;
    }
    public void sendTo(byte[] buf)
    {
        sendTo(buf, buf.length);
    }

    public void sendTo(byte[] buf, int len)
    {
        try
        {
           output.write(buf, 0, len);
            output.flush();
            System.out.println(sockOutput.getPort() + " data to client was send");
        }
        catch(IOException e)
        {
            System.out.println("sendToClient IOExcepetion " + e);
        }
    }

    public void run()
    {
        try {
            input = sockInput.getInputStream();
            output = sockOutput.getOutputStream();
        }
        catch(IOException e)
        {
            System.out.println("DataExchange constructor IOException: " + e);
        }
        dataExchange();
        parent.deleteStream(sockInput);
    }
    public void dataExchange()
    {
        boolean isActive = true;
        int dataLen = 0;
        while(isActive)
        {

            dataLen = checkData();
            if(dataLen > 0)
                sendTo(buffer, dataLen);
            if(dataLen < 0)
                isActive = false;
            Thread.yield();
        }
    Thread.currentThread().interrupt();
    }

    public int checkData()
    {

        int dataLen = 0;
        if(input == null)
            return dataLen;
        else
        {
            try
            {
                dataLen = input.read(buffer);
                System.out.println("From " + sockInput.getInetAddress() + " was read " + dataLen + " bytes");
            }
            catch(IOException e)
            {
                System.out.println("checktData IOException " + e);
            }
            return dataLen;
        }
    }


}
