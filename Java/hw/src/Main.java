import java.io.*;
public class Main {

    public static void main(String[] args) {
        File file = new File("hw.txt");
        try {
            String path = file.getCanonicalPath();
            throw new IOException("My exception");
        }
        catch(IOException e){
            System.out.println("Exception " + e.getMessage());
        }
        finally {
            System.out.println("Finally");
        }

    }
}

