import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class EchoServer {
  public static void main(String[] args) {
    try{
      ServerSocket servSock = new ServerSocket(6255);//create socket at port 6255
      while(true)
      {
        Socket clientSock = servSock.accept();//client socket
        BufferedReader input = new BufferedReader(new InputStreamReader(clientSock.getInputStream()));
        PrintWriter output = new PrintWriter(clientSock.getOutputStream());
        output.print("Connection with server, ");
        output.println("enter STOP to exit.");
        output.flush();
        while(true)
        {
          String str = input.readLine();
          if(str == null)
            break;
          else
          {
            output.println("Server: " + str);
            output.flush();
            if(str.trim().equals("STOP"))
              break;
          }
        }
        input.close();
        output.close();
        clientSock.close();
      }
    }
    catch(IOException ioe) {
      System.out.println("IOException.");
    }
  }
}
