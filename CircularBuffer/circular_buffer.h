#pragma once
#include <algorithm>
#include <iostream>
template <typename value_type>
class CircularBuffer {
private:
    value_type * buffer;
    int actual_start;//actual start position (tail), points to place where is the first element
    int actual_index;//actual end position (head), points to place next to last element
    int cb_capacity;//volume of buffer
    int actual_size;//number of filled elements
    int const index_to_start(int where) const//return index to actual start + where
    {
        if(!cb_capacity)
            throw;
        int normindex = actual_start + where;
        while(normindex < 0)
            normindex += cb_capacity;
        return normindex % cb_capacity;
    }
    int const index_to_end(int where) const//return index to actual end + where
    {
        if(!cb_capacity)
            throw;
        int normindex = actual_index + where;
        while(normindex < 0)
            normindex += cb_capacity;
        return normindex % cb_capacity;
    }
public:
    CircularBuffer(const CircularBuffer & cb)//copy constructor, uses operator = TEST
    {
        buffer = nullptr;//because it's not nullptr before construction
        *this = cb;
    }

    void erase(int first, int last)//first < last else throw TEST
    {
        if(first > last)
            throw;
        if(first >= actual_size || last >= actual_size)//incorrect indexes
            throw;
        int erase_size = last - first + 1;
        if(!first || last == actual_size - 1)//unusual situations
        {
            for (int i = 0; i < erase_size; ++i)//erase from first to (last - 1)
            {
                buffer[index_to_start(first + i)].~value_type();
                buffer[index_to_start(first + i)] = value_type();

            }
            if(!first && last == actual_size - 1)//erase whole buffer
            {
                actual_start = 0;
                actual_index = 0;
            }
            else if(!first)//if erase from actual start not to end
                actual_start = index_to_start(erase_size);
            else if(last == actual_size - 1)//erase not from start but from end
                actual_index = index_to_end(-erase_size);
            actual_size -= erase_size;
        }
        else//usual erase
        {
            for (int i = 0; i < erase_size; ++i)//erase from first to (last - 1)
            {
                buffer[index_to_start(first + i)].~value_type();
                buffer[index_to_start(first + i)] = value_type();
            }
            for (int i = 0; i < actual_size - 1 - last; ++i)//move tail to head
            {
                buffer[index_to_start(first + i)] = buffer[index_to_start(last + i + 1)];
            }
            actual_index = index_to_end(-erase_size);
            actual_size -= erase_size;
        }
    }
    void clear()//erase whole buffer TEST
    {
        for(int i = 0; i < cb_capacity; ++i)
        {
            buffer[i].~value_type();
            buffer[i] = value_type();
        }
        actual_start = 0;
        actual_index = 0;
        actual_size = 0;
    }
    CircularBuffer(): buffer(nullptr), cb_capacity(0), actual_start(0), actual_size(0), actual_index(0)//default constructor
    {
    }
    void swap(CircularBuffer & cb)//swap all content from cb and from *this TEST
    {
        std::swap(cb_capacity,cb.cb_capacity);
        std::swap(actual_index, cb.actual_index);
        std::swap(actual_start, cb.actual_start);
        std::swap(actual_size, cb.actual_size);
        std::swap(buffer, cb.buffer);
    }

    int capacity() const//return capacity
    {
        return cb_capacity;
    }


    explicit CircularBuffer(int capacity)//constructor with capacity TEST
    {
        if(capacity <= 0)//bad range
            throw;
        cb_capacity = capacity;
        buffer = new value_type[cb_capacity];//allocate memory without initialization
        for(int i = 0; i < cb_capacity; ++i)
            buffer[i] = value_type();
        actual_size = 0;
        actual_index = 0;
        actual_start = 0;
    }

    ~CircularBuffer()//destructor
    {
        delete [] buffer;//clear memory
    }


    bool full() const
    {
        return (size() == capacity());//check if size() equals capacity()
    }

    CircularBuffer(int capacity, const value_type &elem)//constructor with capacity and start initialization value TEST
    {
        if(capacity <= 0)//bad range
            throw;
        cb_capacity = capacity;
        actual_size = cb_capacity;//whole buffer is filled
        buffer = new value_type[cb_capacity];//allocate memory
        for(int i = 0; i < cb_capacity; ++i)//initialize whole array with elem
            buffer[i] = elem;
        actual_start = 0;
        actual_index = 0;
    }

    value_type & operator[](int i)//this method return reference to element with position i
    {
        int temp_index = index_to_start(i);//counting from actual start, use circular index
        return buffer[temp_index];
    }

    value_type & at(int i) {
        if(!(i >= 0 && i < actual_size))//check if index is correct
            throw;//exception if no
        return (*this)[i];//element if yes
    }

    const value_type & at(int i) const
    {
        if(!(i >= 0 && i < actual_size))//check if index is correct
            throw;//exception if no
        return (*this)[i];//element if yes
    }

    const value_type & operator[](int i) const//this method doesn't change anything, because it can't
    {
        int temp_index = index_to_start(i);//counting from actual start
        return buffer[temp_index];
    }

    int size() const//return value of size
    {
        return actual_size;
    }

    bool empty() const//return true if empty else false
    {
        return !size();
    }

    value_type & front()//return reference to actual start of data
    {
        return buffer[index_to_start(0)];
    }

    value_type & back()//return reference to actual end of data
    {
        return buffer[index_to_end(-1)];
    }

    const value_type & front() const//return reference to actual start of data
    {
        return buffer[index_to_start(0)];
    }

    const value_type & back() const//return reference to actual end of data
    {
        return buffer[index_to_end(-1)];
    }

    value_type * linearize()//make actual_start = 0 by moving data
    {
        if(!is_linearized()) {
            std::rotate(&buffer[0], &buffer[actual_start], buffer+cb_capacity);//TEST
            actual_start = 0;
            actual_index = actual_size % cb_capacity;
        }
        return &(buffer[0]);
    }

    bool is_linearized() const//return true if buffer is linearized and false else
    {
        return !actual_start;//if 0 then true, if not 0 then false
    }

    void rotate(int new_begin)//TEST
    {
        if(!(new_begin < 0 || new_begin >= actual_size))//if doesn't equal 0
        {
            if(full())//if full just change pointers
            {
                actual_start = index_to_start(new_begin);
                actual_index = index_to_end (new_begin);
                return;
            }
            for(int i = 0; i < new_begin; ++i)//new_begin iterations that move last element to the head
            {
                (*this).push_back(buffer[actual_start]);
                (*this).pop_front();
            }
        }
        else throw;
    }

    int reserve() const//return number of empty places in buffer
    {
        return (cb_capacity - actual_size);
    }

    void push_back(const value_type & item)//add one element (item) on top of buffer TEST
    {
        if(!cb_capacity)
            throw;
        if(full())//buffer is full
        {
            buffer[actual_index] = item;//rewrite oldest element
            actual_index = index_to_end(1);//move pointer to head
            actual_start = actual_index;//move pointer to start, equals actual_index because buffer is full
        }
        else
        {
            buffer[actual_index] = item;//write to empty space
            actual_index = index_to_end(1);//move pointer to head
            actual_size++;//because added one element
        }
    }

    void push_front(const value_type & item = value_type())//add element behind oldest TEST
    {
        if(!cb_capacity)
            throw;
        if(full())//buffer is full
        {
            buffer[index_to_start(-1)] = item;//rewrite element behind first
            actual_start = index_to_start(-1);//move start pointer
            actual_index = actual_start;//move end pointer
        }
        else
        {
            buffer[index_to_start(-1)] = item;//write space behind first element
            actual_start = index_to_start(-1);//move start pointer
            actual_size++;//increment size
        }
    }

    void pop_back()//delete last (newest) element TEST
    {
        if(actual_size)//if have what to delete
        {
            actual_size--;//deleted one element
            buffer[index_to_end(-1)].~value_type();//delete element
            buffer[index_to_end(-1)] = value_type();//make it zero
            actual_index = index_to_end(-1);//move head to older position
        }
        //It doesn't do anything if there are not any elements
    }

    void pop_front()//delete the first (oldest) element TEST
    {
        if(actual_size)//if have what to delete
        {
            actual_size--;//deleted one element
            buffer[index_to_start(0)].~value_type();//delete element
            buffer[index_to_start(0)] = value_type();//make it zero
            actual_start = index_to_start(1);//move tail to newer position
        }
        //It doesn't do anything if there are not any elements
    }

    void insert(int pos, const value_type & item)//insert item to user position TEST
    {
        if(!(pos >= 0 && pos < actual_size))
            throw;
        if(pos == 0)
        {
            if(full())
                pop_front();
            push_front(item);
        }
        else if(pos == actual_size - 1)
        {
            if(full())
                pop_back();
            push_back(item);
        }
        else
        {
            int i = 0;
            if(full())
                pop_front();//if full than don't rewrite prefirst element
            for(; i < pos + 1; ++i)
            {
                buffer[index_to_start(i - 1)] = buffer[index_to_start(i)];//move half of array to one position to left
            }
            if(!full())
            {
                actual_start = index_to_start(-1);//move head to one element left
                actual_size++;//one element added
            }
            buffer[index_to_start(pos)] = item;//insert item
        }
    }

    CircularBuffer & operator=(const CircularBuffer &cb)//operator = TSK нужен ли шаблон в параметре?
    {
        if(this == &cb)//check for this=this
            return *this;

        actual_start = cb.actual_start;
        actual_index = cb.actual_index;
        actual_size = cb.actual_size;
        cb_capacity = cb.cb_capacity;

        delete [] buffer;
        buffer = new value_type[cb_capacity];

        for(int i = 0; i < cb_capacity; ++i)
            buffer[i] = cb.buffer[i];

        return *this;
    }

    void resize(int new_size, const value_type & item)//resize buffer and fill new places by item if new_size > cb_capacity TEST
    {
        if(new_size < 0)
            throw;
        if(new_size == actual_size)
            return;//nothing to do
        else if(new_size < actual_size)
            erase(0, actual_size - new_size);//erase oldest elements
        else
        {
            if(new_size <= cb_capacity)
            {
                for(int i = actual_size; i < new_size; ++i)
                    push_back(item);
            }
            else
            {
                set_capacity(new_size);
                for(int i = actual_size; i < new_size; ++i)
                    push_back(item);
            }
        }
    }

    void set_capacity(int new_capacity)//apply new capacity, buffer recreating TEST
    {
        if(new_capacity == cb_capacity)//nothing to do
            return;
        linearize();
        if(new_capacity < actual_size)
        {
            erase(new_capacity, actual_size - 1);
            actual_size = new_capacity;
            actual_index = 0;
        }
        auto temp = new value_type [actual_size];//temp array
        for(int i = 0; i < actual_size; ++i)//initialize temp array
            temp[i] = buffer[index_to_start(i)];
        delete [] buffer;//clear buffer
        buffer = new value_type [new_capacity];//create new buffer with new capacity
        cb_capacity = new_capacity;
        actual_index = 0;
        int temp_size = actual_size;
        actual_size = 0;//push_back increments actual size
        for(int i = 0; i < temp_size; ++i)
            push_back(temp[i]);
        delete [] temp;
    }
};

template <typename value_type>
bool operator==(const CircularBuffer<value_type> & a, const CircularBuffer<value_type> & b)//TEST
{
    if(a.size() != b.size())
        return false;
    for(int i = 0; i < a.size(); ++i)
    {
        if(a.at(i) != b.at(i))
            return false;
    }
    return true;
}
template <typename value_type>
bool operator!=(const CircularBuffer<value_type> & a, const CircularBuffer<value_type> & b)
{
    return !(a == b);
}