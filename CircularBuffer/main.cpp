#include <iostream>
#include "circular_buffer.h"

template<typename type>
void output_cb(const CircularBuffer<type> &cb);
template <typename  type>
void buf_info(const CircularBuffer<type> &buf);
template<typename type>
void test();

class line
{
private:
    int x;
    int y;
public:
    int get_x()const
    {
        return x;
    }
    int get_y()const
    {
        return y;
    }
    line(): x(0), y(0)
    {
    }
    line(int a, int b)
    {
        x = a;
        y = b;
    }
    explicit line(int a)//it is a point
    {
        x = a;
        y = a;
    }
    line & operator=(const line & ln)
    {
        if(this != &ln)
        {
            this->x = ln.x;
            this->y = ln.y;
        }
        return *this;
    }
    ~line()= default;
    line(const line & tocop)
    {
        (*this) = tocop;
    }
};
bool operator==(const line & one, const line & two)
{
    return (one.get_x() == two.get_x() && one.get_y() == two.get_y());
}
bool operator!=(const line & one, const line & two)
{
    return !(one == two);
}

int main()
{
    test<line>();
    return 0;
}
template<typename type>
void output_cb(const CircularBuffer<type> &cb)
{
    for(int i = 0; i < cb.size(); ++i)
        std::cout << cb.at(i) << " ";
    std::cout << std::endl;
}
template <typename  type>
void buf_info(const CircularBuffer<type> &buf)
{
    using namespace std;
    cout << "Capacity: " << buf.capacity() << endl;
    cout << "Size: " << buf.size() << endl;
    cout << "First: " << buf.front().get_x() << " " << buf.front().get_y() << endl;
    cout << "Last: " << buf.back().get_x() << " " << buf.back().get_y() << endl;
        cout << "Arr: ";
        for(int i = 0; i < buf.size(); ++i)
            cout << " | (" << buf[i].get_x() << ", " << buf[i].get_y() << ")";
        cout  << " | ";
    cout << endl;
}
template<typename type>
void test()
{
    using namespace std;
    CircularBuffer <type> empty_buffer;
    if(empty_buffer.size())
        cout << "Empty buffer error (size)" << endl;
    if(empty_buffer.capacity())
        cout << "Empty buffer error (capacity)" << endl;
    int n = 10, a = 1, b = 5;
    CircularBuffer <type> one(n, line(a, b));//capacity + item constructor
    for(int i = 0; i < 10; i ++)
    {
        if (one[i] == line(a, b))
            continue;
        else
        {
            cout << "Constructor capacity + item error";
            break;
        }
    }
    CircularBuffer <type> two(n);//test capacity constructor
    if(two.capacity() != n)
        cout << "Constructor capcity error";
    if(two.size())
        cout << "Constructor size error";
    two.push_back(line(a, b));
    if(two.size() != 1)
        cout << "Push back size error";
    if(two[0] != line(a, b))
        cout << "Push back initialization error";
    int c = 2, d = 7, e = 8, f = 9;
    two.push_back(line(c,d));
    if(two.size() != 2)
        cout << "Push back size error";
    if(two[0] != line(a, b) && two[1] != line(c, d))
        cout << "Push back initialization error";
    two.push_front(line(e, f));
    if(two.size() != 3)
        cout << "Push back size error";
    if(two[0] != line(e, f) && two[1] != line(a, b) && two[2] != line(c, d))
        cout << "Push back initialization error";
    two.erase(1, 1);
    if(two.size() != 2)
        cout << "Erase size error";
    if(two[0] != line(e, f) && two[1] != line(c, d))
        cout << "Erase error";
    for(int j = 0; j < 6; ++j)
        two.insert(1, line(a, b));
    if(two.size() != 8)
        cout << "Insert size error";
    if(two[0] != line(e, f) && two[6] != line(c, d) && two[7] != line(a,b))
        cout << "Insert error";
    for(int i = 1; i < 6; ++i)
        if(two[i] != line(a,b))
            cout << "Insert error";
    cout<<endl;
    buf_info(two);
}