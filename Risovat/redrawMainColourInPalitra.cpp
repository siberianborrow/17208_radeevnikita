#include "RisovatFunctions.h"
void redrawMainColourInPalitra(cv::Mat& palitraImg, Mcolour& mainColour)
{
    for (int i = WINDOW_PALITRA_WIDTH / 2 - WINDOW_PALITRA_WIDTH / 10; i < WINDOW_PALITRA_WIDTH / 2 + WINDOW_PALITRA_WIDTH / 10; ++i)
    {
        for (int j = WINDOW_PALITRA_HEIGHT / 10; j < WINDOW_PALITRA_HEIGHT / 10 + 2 * WINDOW_PALITRA_WIDTH / 10; ++j)
        {
            palitraImg.at<cv::Vec3b>(j, i)[0] = mainColour.get_blue();
            palitraImg.at<cv::Vec3b>(j, i)[1] = mainColour.get_green();
            palitraImg.at<cv::Vec3b>(j, i)[2] = mainColour.get_red();
        }
    }
}