#include "RisovatFunctions.h"
#include <iostream>
#include <deque>

extern config MainConfig;
void chooseInstrument(std::deque <cv::Mat> & buffer, int x, int y)
{
    switch (MainConfig.get_instrument_num())
    {

        case N_DEFAULT_BRUSH:
            /*std::cout<<"OLD: "<<MainConfig.get_oldx()<<" "<<MainConfig.get_oldy()<<std::endl;
            std::cout<<"    NOW: "<<x<<" "<<y<<std::endl;*/
            defaultBrush(buffer.front(), x, y);
            break;
        case N_CIRCLE_BRUSH:
            circleBrush(buffer.front(), x, y);
            break;
        case N_LINE:
            line(buffer, x, y);
            break;
        case N_BUCKET:
            bucket(buffer.front(), x, y);
            break;
        case N_TRUE_LINE:
            true_line(buffer, x, y);
            break;
        case N_PIPETKA:
            pipetka(buffer.front(), x, y);
            break;
        case N_UNDO:
            if(!MainConfig.get_isClicked())
            {
                contrl_zet(buffer);
                MainConfig.set_isClicked(true);
            }
            break;
        case N_NOTHING:
            break;
        default:
            defaultBrush(buffer.front(), x, y);
            break;
    }
}