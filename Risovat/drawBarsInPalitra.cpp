#include "RisovatFunctions.h"
void drawBarsInPalitra(cv::Mat& palitraImg, Mcolour& MainColour)
{
    auto d_WINDOW_PALITRA_WIDTH = static_cast<double> (WINDOW_PALITRA_WIDTH);
    for(int i = WINDOW_PALITRA_WIDTH / 10; i < WINDOW_PALITRA_WIDTH * 9 / 10; ++i)
    {
        for(int j = -PALITRA_COLOUR_BAR_HEIGHT; j <= PALITRA_COLOUR_BAR_HEIGHT; ++j)
        {
            palitraImg.at<cv::Vec3b>(WINDOW_PALITRA_HEIGHT * 5 / 8 + j, i)[0] = 0;
            palitraImg.at<cv::Vec3b>(WINDOW_PALITRA_HEIGHT * 5 / 8 + j, i)[1] = 0;
            palitraImg.at<cv::Vec3b>(WINDOW_PALITRA_HEIGHT * 5 / 8 + j, i)[2] = (255.0 / (d_WINDOW_PALITRA_WIDTH * 8.0 / 10.0 )) * (static_cast<double>(i) - d_WINDOW_PALITRA_WIDTH / 10.0);

            palitraImg.at<cv::Vec3b>(WINDOW_PALITRA_HEIGHT * 6 / 8 + j, i)[0] = 0;
            palitraImg.at<cv::Vec3b>(WINDOW_PALITRA_HEIGHT * 6 / 8 + j, i)[1] = (255.0 / (d_WINDOW_PALITRA_WIDTH * 8.0 / 10.0 )) * (static_cast<double>(i) - d_WINDOW_PALITRA_WIDTH / 10.0);
            palitraImg.at<cv::Vec3b>(WINDOW_PALITRA_HEIGHT * 6 / 8 + j, i)[2] = 0;

            palitraImg.at<cv::Vec3b>(WINDOW_PALITRA_HEIGHT * 7 / 8 + j, i)[0] = (255.0 / (d_WINDOW_PALITRA_WIDTH * 8.0 / 10.0 )) * (static_cast<double>(i) - d_WINDOW_PALITRA_WIDTH / 10.0);
            palitraImg.at<cv::Vec3b>(WINDOW_PALITRA_HEIGHT * 7 / 8 + j, i)[1] = 0;
            palitraImg.at<cv::Vec3b>(WINDOW_PALITRA_HEIGHT * 7 / 8 + j, i)[2] = 0;
        }
    }
}