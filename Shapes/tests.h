#pragma once
#include <iostream>
#include <string>
#include "parser.h"
#include "shapes.h"

void test_parser_circle(std::istream& in);
void test_parser_square(std::istream& in);
void test_parser_triangle(std::istream& in);
void test_parser(std::istream& in);