#include <cmath>
#include <iostream>
#include "x_y.h"

double x_y::get_y() const
{
    return y;
}
double x_y::get_x() const
{
    return x;
}
double x_y::line_length(const x_y & dot)const
{
    double x = this->get_x() - dot.get_x();//length of x
    double y = this->get_y() - dot.get_y();//length of y
    double length = sqrt(x * x + y * y);//length if line in xOy
    return length;
}

std::istream& operator >> (std::istream& input, x_y& dot)
{
    double x, y;
    input >> x >> y;
    dot.x = x;
    dot.y = y;
    return input;
}