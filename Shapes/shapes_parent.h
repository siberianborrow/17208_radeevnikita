#pragma once
#include <string>
#include "x_y.h"

using std::string;
class shapes_parent
{
private:
    string shape_name;
public:
    shapes_parent():shape_name("Default"){}
    explicit shapes_parent(string str):shape_name(str){}
    string name() const;//return shape's name
    virtual double perimeter() const;//return shape's perimeter
    virtual double area() const;//return shape's square
};

