#include "shapes.h"
double square::perimeter() const
{
    double perimeter = vertex[0].line_length(vertex[1]) * 4;// 4 * a
    return perimeter;
}
double square::area() const
{
    double area = vertex[0].line_length(vertex[1]) * vertex[0].line_length(vertex[1]);// a * a
    return area;
}

std::istream& operator >> (std::istream& input, square& obj)
{
        input >> obj.vertex[0] >> obj.vertex[1] >> obj.vertex[2] >> obj.vertex[3];
        return input;
}