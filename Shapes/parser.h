#pragma once
#include <iostream>
#include <string>
#include "shapes_parent.h"
#include "shapes.h"
int const MAX_LETTERS_WORD = 8;// triangle - 8 letters
int const NUM_OF_SHAPES = 3;//number of shapes
shapes_parent* parser(std::istream& in);