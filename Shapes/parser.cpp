#include "parser.h"
shapes_parent* parser(std::istream& in)//return 0 if key of end file - string "end", 1 else
{
    string toread;
    string shapes_names[NUM_OF_SHAPES] = {"circle" , "square", "triangle"};
    while(true)
    {
        in >> toread;
        if(toread.length() > MAX_LETTERS_WORD)//to not do useless ifs
            continue;
        if(toread == shapes_names[0])
        {
            auto* shape = new circle;
            in >> *shape;
            return shape;
        }
        if(toread == shapes_names[1])
        {
            auto shape = new square;
            in >> *shape;
            return shape;
        }
        if(toread == shapes_names[2])
        {
            auto shape = new triangle;
            in >> *shape;
            return shape;
        }
    }
}