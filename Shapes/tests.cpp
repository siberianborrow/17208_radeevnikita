#include "tests.h"
void test_parser_circle(std::istream& in)
{
    shapes_parent* fortest = parser(in);
    if(fortest->name() == "circle")
        std::cout << "\nshapes_parent* test name: \"" << fortest->name() << "\" DONE\n";
    else
        std::cout << "\nshapes_parent* test name: \"" << fortest->name() << "\" FAIL\n";
    std::cout << "area: " << fortest->area() << std::endl;
    std::cout << "perimeter: " << fortest->perimeter() << std::endl;
    delete fortest;
}
void test_parser_square(std::istream& in)
{
    shapes_parent* fortest = parser(in);
    if(fortest->name() == "square")
        std::cout << "\nshapes_parent* test name: \"" << fortest->name() << "\" DONE\n";
    else
        std::cout << "\nshapes_parent* test name: \"" << fortest->name() << "\" FAIL\n";
    std::cout << "area: " << fortest->area() << std::endl;
    std::cout << "perimeter: " << fortest->perimeter() << std::endl;
    delete fortest;
}
void test_parser_triangle(std::istream& in)
{
    shapes_parent* fortest = parser(in);
    if(fortest->name() == "triangle")
        std::cout << "\nshapes_parent* test name: \"" << fortest->name() << "\" DONE\n";
    else
        std::cout << "\nshapes_parent* test name: \"" << fortest->name() << "\" FAIL\n";
    std::cout << "area: " << fortest->area() << std::endl;
    std::cout << "perimeter: " << fortest->perimeter() << std::endl;
    delete fortest;
}
void test_parser(std::istream& in)
{
    shapes_parent* fortest = parser(in);
    if(fortest->name() == "triangle" || "circle" || "square")
        std::cout << "\nshapes_parent* test name: \"" << fortest->name() << "\" DONE\n";
    else
        std::cout << "\nshapes_parent* test name: \"" << fortest->name() << "\" FAIL\n";
    std::cout << "area: " << fortest->area() << std::endl;
    std::cout << "perimeter: " << fortest->perimeter() << std::endl;
    delete fortest;
}