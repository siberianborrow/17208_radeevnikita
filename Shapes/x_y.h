//it is class of dot, it has methods for working with them
#include <iostream>
#pragma once
class x_y
{
private:
    double x = 0;
    double y = 0;
public:
    x_y():x(0), y(0) {}
    x_y(int x, int y):x(x), y(y) {}//x is x, y is y
    double get_y()const;//return y
    double get_x()const;//return x
    double line_length(const x_y & dot)const;//return length of line [*this, dot]
    friend std::istream& operator >> (std::istream& input, x_y& dot);
};