#include "shapes.h"
#include <cmath>
std::istream& operator >>(std::istream& input, triangle& obj)
{
    input >> obj.vertex[0] >> obj.vertex[1] >> obj.vertex[2];
    return input;
}
double triangle::perimeter() const
{
    double perimeter = vertex[0].line_length(vertex[1]) + vertex[1].line_length(vertex[2]) + vertex[2].line_length(vertex[0]);
    return perimeter;
}
double triangle::area() const
{
    double p = perimeter() / 2.0;
    double square = sqrt(p * (p - vertex[0].line_length(vertex[1])) * (p - vertex[1].line_length(vertex[2])) * (p - vertex[2].line_length(vertex[0])));
    return square;
}