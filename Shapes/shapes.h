//list of available shapes
#pragma once

#include "shapes_parent.h"

class circle: public shapes_parent
{
private:
    double radius = 0;
    x_y center;//useless coordinate for perimeter and area
public:
    circle():shapes_parent("circle"){}
    double perimeter () const final;
    double area () const final;
    friend std::istream& operator >> (std::istream& input, circle& obj);

};

class square : public shapes_parent
{
private:
    x_y vertex[4];
public:
    square(): shapes_parent("square"){}
    double perimeter() const final;
    double area() const final;
    friend std::istream& operator >> (std::istream& input, square& obj);
};

class triangle : public shapes_parent
{
private:
    x_y vertex[3];
public:
    triangle(): shapes_parent("triangle"){}
    double perimeter() const final;
    double area() const final;
    friend std::istream& operator >> (std::istream& input, triangle& obj);
};
