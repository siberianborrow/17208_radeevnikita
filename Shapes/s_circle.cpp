#include <cmath>
#include "shapes.h"
std::istream& operator >> (std::istream& input, circle& obj)
{
    input >> obj.radius >> obj.center;
    return input;
}
double circle::perimeter () const
{
    double perimeter = 2 * M_PI * radius ;
    return perimeter;
}
double circle::area () const
{
    double area = M_PI * radius * radius;
    return area;
}